## 1.0.10 (2023-02-09)

No changes.

## 1.0.9 (2023-02-09)

### Features

- [Added changelog_config.yml](hwakabh/hello-gitlab@5972cb6e9ff4ee147619592af22983f80e19967a)

## 1.0.8 (2023-02-09)

### feature (2 changes)

- [Update msg of release-notes](hwakabh/hello-gitlab@4aa8d4737f354a843dcbbf6175a838f6fd30cd7f)
- [chore: add some message to README.md](hwakabh/hello-gitlab@b96d4e8b84e14258dee02954a49da740a9045136)

## 1.0.7 (2023-02-09)

### feature (1 change)

- [Added comments to gitlab-ci.yml](hwakabh/hello-gitlab@5a547a7bb0134a4be4d0d408ed0274e3179b0da4)

## 1.0.6 (2023-02-09)

No changes.

## 1.0.5 (2023-02-09)

### feature (1 change)

- [Update README.md](hwakabh/hello-gitlab@2f5af331d25373829ddb2c8d21c8f20bf31beba2)
